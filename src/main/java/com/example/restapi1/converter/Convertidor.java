
package com.example.restapi1.converter;

import java.util.ArrayList;
import java.util.List;

import com.example.restapi1.MNota;
import com.example.restapi1.Nota;

import org.springframework.stereotype.Component;

/**
 * Convertidor
 * 
 * @param <MNota>
 * @param <Nota>
 */
@Component("convertidor")
public class Convertidor {
    public List<MNota> convertirLista(List<Nota> notas) {
        List<MNota> mnotas = new ArrayList<>();
        for (Nota nota : notas) {
            mnotas.add(new MNota(nota));
        }
        return mnotas;
    }

}