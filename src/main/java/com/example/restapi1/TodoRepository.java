package com.example.restapi1;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

interface TodoRepository extends CrudRepository<Todo, Long> {

    @Query("SELECT  t.title  FROM Todo t where t.id = :id")
    Optional<String> findTitleById(@Param("id") Long id);

    @Query("SELECT new map(t.title As title)   FROM Todo t")
    List<Object> findTitleAndDesById();

}
