
package com.example.restapi1;

/**
 * MNota
 */
public class MNota {

    public MNota() {

    }

    public MNota(Nota nota) {
        this.id = nota.getId();
        this.nombre = nota.getNombre();
        this.titulo = nota.getTitulo();
        this.contenido = nota.getContenido();
    }

    public MNota(Long id, String nombre, String contenido, String titulo) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.titulo = titulo;
        this.contenido = contenido;

    }

    private Long id;
    private String nombre;
    private String titulo;
    private String contenido;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the contenido
     */
    public String getContenido() {
        return contenido;
    }

    /**
     * @param contenido the contenido to set
     */
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

}