
package com.example.restapi1;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * NotaController
 */
@RestController
@RequestMapping(path = "/nota")
public class NotaController {

    @Autowired
    @Qualifier("servicio")
    NotaService service;

    @PutMapping(path = "/{id}")
    public Nota update(@RequestBody Nota nota, @PathVariable("id") Long id) {
        return service.actualizar(id, nota);

    }

    @PostMapping(consumes = "application/json")
    public Nota postNota(@RequestBody Nota nota) {

        return service.crear(nota);
    }

    @GetMapping
    public List<MNota> getNotas() {
        return service.obtener();
    }

    @GetMapping(value = "/obtenerPorTitulo")
    public List<MNota> getNotasPorTitulo(@RequestParam(value = "titulo") String titulo) {
        return service.obtenerPorTitulo(titulo);
    }

    @PostMapping(value = "/obtenerPorNombreTitulo")
    public MNota postMethodName(@RequestBody Map<String, Object> payload) {

        System.out.println(payload.get("nombre"));
        String nombre = (String) payload.get("nombre");
        String titulo = (String) payload.get("titulo");

        return service.obtenerPorNombreTitulo(nombre, titulo);

    }

}