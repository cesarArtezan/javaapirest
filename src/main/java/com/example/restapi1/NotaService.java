
package com.example.restapi1;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.example.restapi1.MNota;
import com.example.restapi1.converter.Convertidor;

/**
 * NotaService
 */
@Service("servicio")
public class NotaService {
    @Autowired
    @Qualifier("repositorio")
    private NotaRepositorio repositorio;

    @Autowired
    @Qualifier("convertidor")
    private Convertidor convertidor;

    private static final Log logger = LogFactory.getLog(NotaService.class);

    public Nota crear(Nota nota) {
        try {
            return repositorio.save(nota);
        } catch (Exception e) {
            return null;
        }
    }

    public Nota actualizar(Long id, Nota nota) {
        try {
            if (repositorio.existsById(id)) {
                nota.setId(id);
            } else {
                return null;
            }
            return repositorio.save(nota);
        } catch (Exception e) {
            return null;
        }
    }

    public List<MNota> obtener() {
        List<Nota> notasEn = repositorio.findAll();
        logger.info("ALGO 1");
        return convertidor.convertirLista(notasEn);
    }

    public MNota obtenerPorNombreTitulo(String nombre, String titulo) {
        return new MNota(repositorio.findByNombreAndTitulo(nombre, titulo));
    }

    public List<MNota> obtenerPorTitulo(String titulo) {
        return convertidor.convertirLista(repositorio.findByTitulo(titulo));

    }

}