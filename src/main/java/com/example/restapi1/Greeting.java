package com.example.restapi1;

/**
 * Greeting
 */
public class Greeting {

    private final long id;
    private final String content;
    private final String date1;

    public Greeting(long id, String content, String date) {
        this.id = id;
        this.content = content;
        this.date1 = date;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
    public String getDate() {
        return date1;
    }
}