package com.example.restapi1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * GreetingController
 */
@RestController
public class GreetingController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        LocalDate localDate = LocalDate.now();// For reference
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/LLLL/yyyy");
        String formattedString = localDate.format(formatter);
        System.out.println(localDate); // Display the current date
         Greeting x = new Greeting(counter.incrementAndGet(), String.format(template, name), formattedString);

        System.out.println(x.getId());

        return new Greeting(counter.incrementAndGet(), String.format(template, name), formattedString);
    }

}