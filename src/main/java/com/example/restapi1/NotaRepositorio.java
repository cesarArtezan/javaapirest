
package com.example.restapi1;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * NotaRepositorio
 */
@Repository("repositorio")
public interface NotaRepositorio extends JpaRepository<Nota, Long> {
    public abstract List<Nota> findByTitulo(String titulo);

    public abstract Optional<Nota> findById(Long id);

    public abstract List<Nota> findByNombre(String nombre);

    public abstract Nota findByNombreAndTitulo(String nombre, String titulo);

}