package com.example.restapi1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Nota
 */
@Entity
@Table(name = "NOTA")
public class Nota {
    public Nota() {

    }

    public Nota(Long id, String nombre, String contenido, String titulo) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.titulo = titulo;
        this.contenido = contenido;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "titulo")
    private String titulo;

    @Column(name = "contenido")
    private String contenido;

    public String getContenido() {
        return this.contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}