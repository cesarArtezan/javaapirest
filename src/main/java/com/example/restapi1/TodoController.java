package com.example.restapi1;

import java.util.List;
import java.util.Optional;

// import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/todo")
public class TodoController {

    @Autowired
    private TodoRepository repository;

    @GetMapping
    public Iterable<Todo> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Optional<String> findByName(@PathVariable("id") Long id) {

        return repository.findTitleById(id);
    }

    @GetMapping(path = "/title")
    public List<Object> findTitleandDesById() {

        return repository.findTitleAndDesById();
    }

    @PostMapping(consumes = "application/json")
    public Todo create(@RequestBody Todo user) {
        return repository.save(user);
    }

    @PutMapping(path = "/{id}")
    public Todo update(@PathVariable("id") Long id, @RequestBody Todo todo) throws BadHttpRequest {
        if (repository.existsById(id)) {
            Todo todoOld = repository.findById(id).get();
            todo.setDate(todoOld.getDate());
            todo.setId(id);

            return repository.save(todo);
        } else {
            throw new BadHttpRequest();
        }
    }

}