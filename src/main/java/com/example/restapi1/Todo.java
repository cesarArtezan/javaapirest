
package com.example.restapi1;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "todos")
public class Todo {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "creation_time", nullable = false, updatable = false)
  private Date creationTime;

  @Column(name = "description", length = 500)
  private String description;

  @Column(name = "modification_time")
  private Date modificationTime;

  @Column(name = "title", nullable = false, length = 100)
  private String title;

  @PrePersist
  void createdAt() {
    this.creationTime = this.modificationTime = new Date();
  }

  @PreUpdate
  void updatedAt() {
    this.modificationTime = new Date();
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setDate(Date date) {
    this.creationTime = date;
  }

  public Date getDate() {
    return creationTime;
  }

}