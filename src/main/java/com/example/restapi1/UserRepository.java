package com.example.restapi1;

import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

 // import com.example.restapi1.entity.User;

/* 
Publico:
@RepositoryRestResource(path = "/users")
public interface UserRepository extends JpaRepository<User, String> {

} */
/**
 * Privado
 */
@RestResource(exported = false)
public interface UserRepository extends JpaRepository<User, Integer> {

	// User getUserById(Integer userId);

}